# dog.lookup.Dog

A command-line DNS client.

## Building
Generate cargo-sources.json using [flatpak-cargo-generator](https://github.com/flatpak/flatpak-builder-tools/tree/master/cargo).
```sh
git clone https://github.com/ogham/dog
python3 ./flatpak-cargo-generator.py ./dog/Cargo.lock -o cargo-sources.json
```

Build and install with `flatpak-builder`.
```
flatpak-builder --user --install build --force-clean dog.lookup.Dog.yml
```
